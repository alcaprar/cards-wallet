
it('Should have "Login" when not logged in.', () => {
    cy.visit("/");

    cy.get('#navbar')
        .contains('Login')
})

it('Should have register when not logged in.', () => {
    cy.visit("/");

    cy.get('#navbar')
        .contains('Register')
})


it('Should NOT have "My cards" when not logged in.', () => {
    cy.visit("/");

    cy.get('#navbar')
        .contains('My cards')
        .should('not.exist')
})

it('Should NOT have "User" when not logged in.', () => {
    cy.visit("/");

    cy.get('#navbar')
        .contains('User')
        .should('not.exist')
})

it('Should redirect to /login when trying /my-cards but not logged in', () => {
    cy.visit(`/my-cards`);

    cy.log(cy.url())

    cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/login')
    })
})