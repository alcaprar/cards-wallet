import {  } from "../support/helpers";

it('Should show no cards when new user logs in.', () => {
    cy.registerRandomUser();
    cy.loginRandomUser();

    cy.get('#cards').children().should('have.length', 0);
})

it.only('Should show one card successfully when it exists.', () => {
    cy.registerRandomUser();
    cy.loginRandomUserInBackground();

    cy.createCard();

    cy.loginRandomUser();
    cy.get('#cards').children().should('have.length', 1);
})