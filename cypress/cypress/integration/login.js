import { generateRandomEmail, generateRandomString, registerRandomUser } from "../support/helpers";

it('Should redirect to /my-cards when login successfully', () => {
    cy.registerRandomUser();
    cy.get("@user").then(({email, password}) => {
        cy.visit(`/login`);

        cy.get('#input-email').type(email);
        cy.get('#input-password').type(password);

        cy.get('#submit').click();

        cy.location().should((loc) => {
            expect(loc.pathname).to.eq('/my-cards')
        })
    });
})

it('Should NOT redirect to /my-cards when login fails', () => {
    cy.visit(`/login`);

    cy.get('#input-email').type(generateRandomEmail());
    cy.get('#input-password').type(generateRandomString(10));

    cy.get('#submit').click();

    cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/login')
    })
})