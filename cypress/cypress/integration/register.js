import { generateRandomEmail, generateRandomString, registerRandomUser } from "../support/helpers";

it('Should redirect to /my-cards when register successfully', () => {
    cy.visit(`/register`);

    const email = generateRandomEmail();
    const password = generateRandomString(10);

    cy.get('#input-email').type(email);
    cy.get('#input-password').type(password);

    cy.get('#submit').click();

    cy.location().should((location) => {
        expect(location.pathname).to.eq('/my-cards')
    })
})

it('Should fail to register the user twice', () => {
    cy.registerRandomUser();
    
    cy.get("@user").then((response) => {
        cy.log(response);
        const { email, password } = response;

        cy.visit(`/register`);

        cy.get('#input-email').type(email);
        cy.get('#input-password').type(password);

        cy.get('#submit').click();

        cy.location().should((location) => {
            expect(location.pathname).to.eq('/register')
        })

        cy.get('#error').should('be.visible')
    });
})