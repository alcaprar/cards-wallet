import { generateRandomEmail, generateRandomString } from "./helpers";

Cypress.Commands.add('registerRandomUser', () => {
    const email = generateRandomEmail();
    const password = generateRandomString(10);

    cy.request('POST', `${Cypress.env('apiBaseUrl')}/api/auth/local/register`, {
        username: email,
        email,
        password
    }).then(() => {
        cy.wrap({
            email,
            password
        }).as("user")
    })
})

Cypress.Commands.add('loginRandomUserInBackground', () => {
    cy.get("@user").then(({ email, password }) => {
        cy.request('POST', `${Cypress.env('apiBaseUrl')}/api/auth/local`, {
            identifier: email,
            password
        }).then((response) => {
            cy.wrap(`Bearer ${response.body.jwt}`).as("AuthorizationHeader")
        })
    })
})

Cypress.Commands.add('loginRandomUser', () => {
    cy.get("@user").then(({email, password}) => {
        cy.visit(`/login`);

        cy.get('#input-email').type(email);
        cy.get('#input-password').type(password);

        cy.get('#submit').click();

        cy.location().should((location) => {
                expect(location.pathname).to.eq('/my-cards')
        }).then(() => {
            cy.wrap(localStorage.getItem("auth._token.local")).as("AuthorizationHeader")
        })
    })
})

Cypress.Commands.add('createCard', (name, code) => {
    name = name || generateRandomString(10);
    code = code || generateRandomString(10);

    cy.get("@AuthorizationHeader").then((jwt) => {
        cy.request({
            method: 'POST',
            url: `${Cypress.env('apiBaseUrl')}/api/cards`, body: {
                data: {
                    name,
                    code
                }
            },
            headers: {
                Authorization: `${jwt}`
            }
        })
    })
})
