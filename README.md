## Prerequisites

- Node and Yarn
- Run `yarn install` in `cypress` folder.


## How to develop locally

Start the backend and the frontend with docker-compose:

```
docker-compose up
```

Then start cypress (install it first and make sure to have the [prerequisites](https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements)):

```
cd cypress && yarn install && ./node_modules/cypress/bin/cypress open
```

### Fix file permissions with strapi generated files

When strapi generates files when it is running via docker-compose it creates them using `root`. In order to to fix this you need to run: `sudo chown -R $(whoami) backend`.

### Local admin

Email: `admin@cardswallet.com`
Password: `Qwerty1234!`


## Alternatives

- [Loyalty card locker](https://github.com/brarcher/loyalty-card-locker) (not maintained anymore)
- [Catima](https://github.com/CatimaLoyalty/Android)
