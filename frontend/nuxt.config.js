import config from "./config";

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'frontend',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: `${config.api.baseUrl}/api/auth/local`,
            method: 'post',
            propertyName: 'jwt'
          },
          user: {
            url: `${config.api.baseUrl}/api/users/me`,
            method: 'get',
            propertyName: false
          },
          logout: false
        },
        autoFetchUser: true
      }
    }
  },

  publicRuntimeConfig: {
    api: {
      baseUrl: config.api.baseUrl
    }
  },
  privateRuntimeConfig: {

  },

  router: {
    middleware: ['auth']
  },
}
