#!/bin/bash


case $1 in
start)
    docker-compose up -d;;
stop)
    docker-compose down;;
open-cypress)
    (cd cypress && ./node_modules/cypress/bin/cypress open);;
run-tests)
    files=""
    if [ -n "$2" ]; then
        files="--spec cypress/integration/${2}"
    fi

    (cd cypress && ./node_modules/cypress/bin/cypress run $files);;
*)
  secho "Command not known."
esac