'use strict';

/**
 *  card controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::card.card', ({strapi}) => ({
  async create(ctx) {
    const userId = ctx.state.user.id;
    if (!userId) {
      console.log("user not logged in.");
      return null;
    }

    ctx.request.body.data.user = userId;

    return super.create(ctx);
  },

  async find(ctx) {
    const userId = ctx.state.user.id;
    if (!userId) {
      console.log("user not logged in.");
      return {data: [], meta: null}
    }

    // some custom logic here
    ctx.query = { ...ctx.query, user: userId }

    // Calling the default core action
    const { data, meta } = await super.find(ctx);

    console.log(data);

    return { data, meta };
  },
}));
