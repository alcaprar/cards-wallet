module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '1f27e47e48ad9de64fe7500754f2aa2e'),
  },
  watchIgnoreFiles: [
    '**/config/sync/**',
  ],
});
